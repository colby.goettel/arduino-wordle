#include <Adafruit_BusIO_Register.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_I2CRegister.h>
#include <Adafruit_SPIDevice.h>

#include <gamma.h>
#include <RGBmatrixPanel.h>

#include <Adafruit_GFX.h>
#include <Adafruit_GrayOLED.h>
#include <Adafruit_SPITFT_Macros.h>
#include <Adafruit_SPITFT.h>
//#include <gfxfont.h>
//#include <Fonts/Org_01.h> // 5x5
#include <Fonts/Picopixel.h> // 5x3
//#include <Fonts/TomThumb.h> // 5x3
//#include <Fonts/Tiny3x3a2pt7b.h> // 3x3

#include "words.h"

/*
   Arduino Wordle

   This is a Wordle clone that runs on an Arduino UNO and prints to an RGB LED matrix.

   Wiring instructions: https://learn.adafruit.com/32x16-32x32-rgb-led-matrix/new-wiring
*/

// RGB LED matrix definitions
#define CLK 8 // USE THIS ON ARDUINO UNO, ADAFRUIT METRO M0, etc.
#define OE 9
#define LAT 10
#define A A0
#define B A1
#define C A2

RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false);

// Color definitions
#define RED matrix.Color333(7,0,0)
#define GREEN matrix.Color333(0,7,0)
#define ORANGE matrix.Color333(7,2,0)
#define YELLOW matrix.Color333(7,7,0)
#define WHITE matrix.Color333(7,7,7)

// Other definitions
#define DEBUG 0

// Initialize variables
int count = 0;
int x = 0;
int y = 4;

// Possible words to guess
int number_of_words = 1899; /* indexed at 0 */ /* 1899 is 90% program storage space (29180B) */
char random_word[6]; /* length of word (5) plus null terminator */

// Initialize. Runs on boot or reset.
void setup()
{
  randomSeed(analogRead(0));

  // Pick random word
  long random_number = random(number_of_words);
  strcpy_P(random_word, (char *)pgm_read_word(&(word_list[random_number])));

  // Initialize RGB matrix
  matrix.begin();
  matrix.setRotation(45); // sideways (90 is flipped. I don't know either)
  matrix.setCursor(0, 0); // top left
  matrix.setTextSize(1, 0); // smallest size, no magnification
  matrix.setFont(&Picopixel);

  // Begin serial output, print header
  Serial.begin(9600);
  while (!Serial); // wait until Serial is ready
  print_header();
}

/*
   main loop

   Read a character from serial input and print it
   If the word is in the dictionary, check against the current random word and print:
   - green if correct letter in correct place
   - yellow if correct letter but wrong place
   - white otherwise

   Allow six tries

   Sources:
   - Serial code adapted from: https://learn.adafruit.com/adafruit-arduino-lesson-5-the-serial-monitor/the-serial-monitor
   - Storing array in PROGMEM: https://www.arduino.cc/reference/en/language/variables/utilities/progmem/
*/
void loop()
{
  if (Serial.available())
  {
    String input = Serial.readString();
    if ( input.length() != 5 )
    {
      Serial.println("ERROR: Please enter a five character word");
      return;
    }

    // Convert word to char[]
    char word[6];
    input.toCharArray(word, 6);
    Serial.println("user input: " + (String)word);

    if ( count < 6 && okay(word) )
    {
      int location = 0;
      for ( char letter : word )
        print_letter(letter, check_letter(letter, location++));
      
      print_new_line();
      count++;
    }
  }
}

/*
 * function: check_letter
 * 
 * Given a letter, check if it is the correct letter for the given place.
 * 
 * Arguments:
 * - (char)letter: the letter to check
 * - (int)place: the place in the word to check against
 * Returns:
 * - 0: not in word (WHITE)
 * - 1: in word, not in correct place (YELLOW)
 * - 2: in word, in correct place (GREEN)
 */
int check_letter(char letter, int place)
{
  if ( random_word[place] == letter )
   return 2;

  for ( char current_letter : random_word )
    if ( current_letter == letter )
      return 1;
  
  // else
  return 0;
}

/*
 * function: okay
 * 
 * Check if word is in list of allowable input words
 * 
 * Arguments:
 * - (string)word: the word we're checking
 * Returns:
 * - false: word is not in list
 * - true: word is in list
 */
bool okay(char word[])
{
  char current_word[6];
  for ( int i = 0; i <= number_of_words; i++ )
  {
    // Grab current_word from PROGMEM
    strcpy_P(current_word, (char *)pgm_read_word(&(word_list[i])));
    if ( (String)current_word == word )
      return true;
  }

  Serial.println("ERROR: Invalid word");
  return false;
}

/*
 * function: print_header
 * 
 * Prints the game header to Serial.
 * 
 * Arguments: <>
 * Returns: <>
 */
void print_header()
{
  Serial.println("Wordle Arduino clone");
  Serial.println();
  Serial.println("Green: correct letter, correct place");
  Serial.println("Yellow: correct letter, wrong place");
  Serial.println("White: letter not in word");
  Serial.println();

  if ( DEBUG ) Serial.println("random_word: " + (String)random_word);
}

/*
 * function: print_letters
 * 
 * Given a string of letters, print the word to the matrix.
 * 
 * Arguments:
 * - (string)input: the string to print
 * - (int)color: the color to print:
 *   - 0: WHITE
 *   - 1: YELLOW
 *   - 2: GREEN
 * Returns: <>
 */
void print_letter(char letter, int color)
{
  if ( DEBUG ) Serial.println("letter, color: " + (String)letter + ", " + (String)color);
  switch(color)
  {
    // matrix.drawChar format:
    // x_val, y_val, char, color, background (0 is black), font magnification
    case 0:
      matrix.drawChar(x, y, letter, WHITE, 0, 1);
      break;
    case 1:
      matrix.drawChar(x, y, letter, YELLOW, 0, 1);
      break;
    case 2:
      matrix.drawChar(x, y, letter, GREEN, 0, 1);
      break;
    default:
      Serial.println("ERROR: invalid color argument in function print_letters: print_letters(" + (String)letter + ", " + (String)color + ")");
  }

  // Add spacing for the next letter to be printed.
  if ( letter == 'm' || letter == 'w' )
    x += 5;
  else if ( letter == 'i' || letter == 'j' || letter == 'l' || letter == 'r' )
    x += 2;
  else
    x += 3;
}

/*
 * function print_new_line
 * 
 * Reset x and add to y to put us on a newline
 * 
 * Arguments: <>
 * Returns: <>
 */
void print_new_line()
{
  x = 0;
  y += 5;
}
